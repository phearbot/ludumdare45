﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
	bool open = false;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (open)
			this.gameObject.SetActive(false);
    }

	public void Open()
	{
		open = true;
	}
}
