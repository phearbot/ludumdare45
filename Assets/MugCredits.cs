﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class MugCredits : Interactable
{
	ParticleSystem[] particles;
	bool used;
	public GameObject[] ObjectsToDisableOnInteract;
	public GameObject[] ObjectsToEnableOnInteract;

	public MeshCollider colliderToDisable;
	GameManager gameManager;

	//public bool angleToPlayer = false;
	//float startYAngle;
	GameObject player;

	// Start is called before the first frame update
	void Start()
	{
		particles = GetComponentsInChildren<ParticleSystem>();
		DisableParticles();
		used = false;
		gameManager = FindObjectOfType<GameManager>();

		player = GameObject.Find("Player");
		//startYAngle = transform.parent.rotation.eulerAngles.y;
	}

	// Update is called once per frame
	void Update()
	{
		//      if (angleToPlayer)
		//{

		//	print(newVector);
		//}
	}

	public override void TriggerEnter(Collider other)
	{
		if (other.tag == "MainCamera" && !used)
			EnableParticles();
	}



	public override void TriggerExit(Collider other)
	{
		if (other.tag == "MainCamera")
			DisableParticles();
	}



	private void EnableParticles()
	{
		foreach (var particle in particles)
		{
			particle.Play();
		}
	}

	private void DisableParticles()
	{
		foreach (var particle in particles)
		{
			particle.Stop();
		}
	}

	public override void Interact()
	{
		used = true;
		DisableParticles();
		GetComponent<Animator>().SetTrigger("Interact");

		gameManager.Call_ForceLookAt(this.gameObject, true);

		//colliderToDisable.enabled = false;
		//angleToPlayer = true;




		var newVector = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
		transform.parent.LookAt(newVector);

		player.GetComponent<FirstPersonController>().disableMovement = true;

		//foreach (var item in ObjectsToEnableOnInteract)
		//{
		//	item.SetActive(true);
		//}

		//foreach (var item in ObjectsToDisableOnInteract)
		//{
		//	item.SetActive(false);
		//}
	}

	public void AddMinimap()
	{

		gameManager.Call_Anim_MiniMap();
	}

	public void StopInteraction()

	{

		gameManager.FirstPersonController.EndForceLookAT();

	}
}