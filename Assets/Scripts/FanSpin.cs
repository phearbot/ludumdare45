﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanSpin : MonoBehaviour
{
    public float X = 0;
    public float Y = 0;
    public float Z = 0;
    public GameObject fan;

    // Update is called once per frame
    void Update()
    {
        fan.transform.Rotate(X, Y, Z);
    }
}
