﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    Animator Anim_CameraShake;
    Animator Anim_MiniMap;
    Animator Anim_HealthBar;
    GameObject FPSCamera;
    AudioManager am;
    Tuwwet tuwwet;
    PlayerInteraction playerInteraction;

    [HideInInspector]
    public FirstPersonController FirstPersonController;
    [Header("Health")]
    public int currentHealth;
    public int maxHealth = 10;
    public bool healthBarPickedUp = false;
    [Header("HealthBar Sprites")]
    Image healthBar;
    public Sprite[] HealthSprites = new Sprite[11];
    // There's absolutely a better way to do this.
    private int healthSpriteToDisplay = 10;

    // Timer for preventing multiple calls to take damage from a single laser
    float invuln = 1f;
    float timerCurrent  = 1f;
    float timerGameStart = 0f;

    // Bools to trigger music and audio events
    bool bGMTrigger1 = false;
    bool turretRoomTrigger1 = false;

    bool pickedUpGun = false;



	[Header("Timed Door")]
	public Door timedDoor;
	public float doorOpensAt;
	float doorTimer = 0;
	bool doorOpen = false;

	public Door killDoor;
	public GameObject creditsPanel;

	// Start is called before the first frame update
	void Start()
    {
        //Get the Animator attached to the GameObject you are intending to animate.
        FPSCamera = GameObject.FindGameObjectWithTag("MainCamera");
        Anim_CameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        Anim_MiniMap = GameObject.FindGameObjectWithTag("MiniMap").GetComponent<Animator>();
        Anim_HealthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<Animator>();
        FirstPersonController = GameObject.FindObjectOfType<FirstPersonController>();
        playerInteraction = GameObject.FindObjectOfType<PlayerInteraction>();
        tuwwet = GameObject.FindObjectOfType<Tuwwet>();
        am = GameObject.FindObjectOfType<AudioManager>(); 
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        currentHealth = maxHealth;
        healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<Image>();
        healthBar.overrideSprite = HealthSprites[healthSpriteToDisplay];
    }



    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && pickedUpGun == true)
        {
            print("gun Fired");
            pickedUpGun = false;
            // audio que.
            am.Play("GunShot1");
            am.Play("GunShot2");
            am.Play("GunShot3");
            am.Play("GunShot4");
            // flash screen for gun shot.
            Anim_CameraShake.ResetTrigger("GunShot");
            Anim_CameraShake.SetTrigger("GunShot");
            FirstPersonController.forceLookAt = false;
            tuwwet.PlayerShotGun();
            playerInteraction.zapper.gameObject.SetActive(false);

			killDoor.Open();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Call_Anim_HealthBar();
        }

        timerGameStart += Time.deltaTime;

		// Handling the first door timer
		if (!doorOpen)
		{
			doorTimer += Time.deltaTime;
			if (doorTimer > doorOpensAt)
			{
				timedDoor.Open();
				doorOpen = true;
				BGMTrigger1();
			}
		}
    }

	public void RollCredits()
	{
		creditsPanel.SetActive(true);
		FirstPersonController.disableMovement = true;

	}

    public void Call_ForceLookAt(GameObject ForceLookAt_X, bool disableMovement)
    {
        FirstPersonController.CallForceLookAt(ForceLookAt_X);
        FirstPersonController.disableMovement = disableMovement;
    }

    public void Call_Anim_MiniMap()
    {
        Anim_MiniMap.SetTrigger("MiniMap");
    }

    public void Call_Anim_HealthBar()
    {
        Anim_HealthBar.ResetTrigger("HealthBar");
        Anim_HealthBar.SetTrigger("HealthBar");
        Invoke("HealthBarPickedUp", 2f);
    }

    public void HealthBarPickedUp()
    {
        healthBarPickedUp = true;
    }

    // Should only be called when taking damage
    public void Call_Anim_CameraShake()
    {
        Anim_CameraShake.ResetTrigger("CameraShake");
        Anim_CameraShake.SetTrigger("CameraShake");
    }

    public void TakeDamage()
    {
        
        if (healthBarPickedUp == true && currentHealth > 0  && timerGameStart > timerCurrent)
        {
            tuwwet.hitRegistered = true;
            currentHealth--;
            Call_Anim_CameraShake();
            healthSpriteToDisplay--;
            healthBar.overrideSprite = HealthSprites[healthSpriteToDisplay];
            print("take damage called");
            timerCurrent = timerGameStart + invuln;
            am.Play("PlayerDamage");

            if (currentHealth <= 0)
            {
                PlayerDeath();
            }
        }
    }

    public void PlayerDeath()
    {
        FirstPersonController.disableMovement = true;
        Invoke("Reload", 3f);
        Anim_CameraShake.ResetTrigger("PlayerDeath");
        Anim_CameraShake.SetTrigger("PlayerDeath");
        am.Play("PlayerDeath");
        am.FadeoutBGM("BGM1");
    }

    void Reload()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    // Audio Methods
    public void BGMTrigger1()
    {
        if(bGMTrigger1 == false)
        {
            bGMTrigger1 = true;
            print("BGMTrigger1 - ACTIVE");
            am.FadeinBGM("BGM1");
        }
    }

    public void TurretRoomTrigger1()
    {
        if(turretRoomTrigger1 == false)
        {
            turretRoomTrigger1 = true;
            Invoke("TurretIsAttacking", 2f);
        }

    }

    void TurretIsAttacking()
    {
        tuwwet.playerEnteredTheRoom = true;
        tuwwet.attackingPlayer = !tuwwet.attackingPlayer;
    }

    public void PlayerPickedUpGun()
    {
        tuwwet.PlayerPickedUpGun();
        Invoke("GunIsInteractable", 3f);
        Call_ForceLookAt(tuwwet.gameObject, false);
    }

    void GunIsInteractable()
    {
        pickedUpGun = true;
    }

}
