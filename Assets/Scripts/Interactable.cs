﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{

		TriggerEnter(other);
	}

	private void OnTriggerExit(Collider other)
	{


		TriggerExit(other);
	}

	public abstract void Interact();

	public abstract void TriggerEnter(Collider other);

	public abstract void TriggerExit(Collider other);

}
