﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lasoww : MonoBehaviour
{
	float lifetime = 5f;
	float timer = 0f;

    public AudioClip[] audioClips = new AudioClip[16];
    AudioSource audioSource;
    AudioClip choosenAudioClip;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        int index = Random.Range(0, audioClips.Length);
        choosenAudioClip = audioClips[index];
        audioSource.clip = choosenAudioClip;
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
		timer += Time.deltaTime;
		if (timer > lifetime)
			Destroy(gameObject);

        
		transform.position += transform.forward * Time.deltaTime * 40;
    }
}
