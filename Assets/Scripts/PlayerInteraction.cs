﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
	public Interactable currentTarget;
    GameManager gm;

	public GameObject zapper;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();  
    }

    // Update is called once per frame
    void Update()
    {
		if (currentTarget != null && Input.GetMouseButtonDown(0))
			currentTarget.Interact();
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Interactable")
		{
			currentTarget = other.GetComponent<Interactable>();
		}

        if (other.tag == "Laser")
        {
            gm.TakeDamage();
        }

        if (other.tag == "BGMTrigger1")
        {
            print("Trigger has been called = " + other.tag);
            gm.BGMTrigger1();
        }

        if (other.tag == "TurretRoomTrigger1")
        {
            print("Trigger has been called = " + other.tag);
            gm.TurretRoomTrigger1();
        }

        if (other.tag == "Gun")
        {
			print("Collided with gun");
            gm.PlayerPickedUpGun();
			zapper.SetActive(true);
            Destroy(other.gameObject);
        }

		if (other.tag == "Credits")
			gm.RollCredits();
    }    

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Interactable")
		{
			currentTarget = null;
            
		}
	}

}
