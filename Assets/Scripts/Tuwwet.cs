﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuwwet : MonoBehaviour
{
	[Range(.1f,2)]
	public float fireRate;
	public GameObject projectile;
	public GameObject turretHead;
	public GameObject emitter;

    public bool attackingPlayer = false;

	float timer = 0;
	GameObject playerCamera;

    public AudioClip[] audioClips = new AudioClip[16];
    AudioSource audioSource;
    AudioClip choosenAudioClip;

    // bools for turret dialog
    public bool playerEnteredTheRoom = false;
    public bool playerPickedUpTheGun = false;
    bool turretIsTalking = false;
    bool introStatement = false;
    public bool hitRegistered = false;
    int dialogIndex;

    // Timer for turret dialog
    //float timerBetweenPhrases = 2f;
    float timerTalking = 0f;
    float timerCurrent;

    // Start is called before the first frame update
    void Start()
    {
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
		timer += Time.deltaTime;

		if (attackingPlayer && timer > fireRate)
		{
			Instantiate(projectile, emitter.transform.position, turretHead.transform.rotation);
			timer = 0;
		}

		LookAtPlayer();
        TurretDialog();
    }

    
	void LookAtPlayer()
	{
		turretHead.transform.LookAt(playerCamera.transform);
		
	}

    void TurretDialog()
    {
        if (turretIsTalking == true)
        {
            timerCurrent += Time.deltaTime;
            if (timerCurrent >= timerTalking)
            {
                turretIsTalking = false;
                timerCurrent = 0;
            }
        }

        if (playerEnteredTheRoom == true)
        {
            // intro dialog
            if (introStatement == false)
            {
                print("TurretDialog intro statement is being called");
                dialogIndex = Random.Range(0, 4);
                introStatement = true;
                turretIsTalking = true;
                PlayTurretDialog();
            }

            // hit registered dialog - should stop anything else he is saying to announce you've taken damage.
            else if (hitRegistered == true)
            {
                print("Hit Dialog called");
                audioSource.Stop();
                dialogIndex = Random.Range(19, 25);
                hitRegistered = false;
                turretIsTalking = true;
                PlayTurretDialog();
            }

            // attacking dialog
            else if (playerPickedUpTheGun == false && turretIsTalking == false)
            {
                print("TurretDialog attacking dialog is being called");
                dialogIndex = Random.Range(6, 16);
                turretIsTalking = true;
                PlayTurretDialog();
            }
            
            // gun picked up dialog
            else if (playerPickedUpTheGun == true && turretIsTalking == false)
            {
                print("TurrentDialog player has gun called");
                dialogIndex = Random.Range(28, 34);
                turretIsTalking = true;
                PlayTurretDialog();
            }
            
        }
    }

    void PlayTurretDialog()
    {   
        choosenAudioClip = audioClips[dialogIndex];
        audioSource.clip = choosenAudioClip;
        audioSource.Play();
        timerTalking = audioSource.clip.length;
        timerCurrent = 0;
    }

    public void PlayerPickedUpGun()
    {
        audioSource.Stop();
        attackingPlayer = false;
        playerPickedUpTheGun = true;
        turretIsTalking = false;
    }

    public void PlayerShotGun()
    {
        Destroy(this.gameObject);
    }
}
